package com.example.demo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Task5810Controller {
	@CrossOrigin
	@GetMapping("/ducks")
	private Duck getDuck() {
		Duck myDuck = new Duck();
		return myDuck;
	}
	
	@CrossOrigin
	@GetMapping("/fishes")
	private Fish getFish() {
		return new Fish(5,"female",6,true);
	}
	
	@CrossOrigin
	@GetMapping("/zebras")
	private Zebra getZebra() {
		return new Zebra();
	}
}
