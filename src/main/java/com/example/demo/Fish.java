package com.example.demo;

public class Fish {
	public Fish(int age, String gender, int size, boolean canEat) {
	
		this.age = age;
		this.gender = gender;
		this.size = size;
		this.canEat = canEat;
	}

	public Fish() {
		this.age = 1;
		this.gender = "male";
		this.size = 2;
		this.canEat = true;
	}
	private int age;
	private String gender;
	private int size;
	private boolean canEat;

	private boolean isMammal() {
		return false;
	}

	private void mate() {
		System.out.println("Fish mate");
	}

	private void siwm() {
		System.out.println("Fish swimming");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isCanEat() {
		return canEat;
	}

	public void setCanEat(boolean canEat) {
		this.canEat = canEat;
	}

}
