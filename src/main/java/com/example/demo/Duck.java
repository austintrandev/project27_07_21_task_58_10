package com.example.demo;

public class Duck {
	public Duck(int age, String gender, String beakColor) {
		this.age = age;
		this.gender = gender;
		this.beakColor = beakColor;
	}

	public Duck() {
		this.age = 1;
		this.gender = "female";
		this.beakColor = "yellow";
	}

	private int age;
	private String gender;
	private String beakColor;

	private boolean isMammal() {
		return false;
	}

	private void mate() {
		System.out.println("Duck mate");
	}

	private void siwm() {
		System.out.println("Duck swimming");
	}

	private void quack() {
		System.out.println("Duck quack");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBeakColor() {
		return beakColor;
	}

	public void setBeakColor(String beakColor) {
		this.beakColor = beakColor;
	}

}
